Supervision API - Api for implementation of the time tracking system. The web version is made with React/Redux/saga stack.
The reader is made on the basis of the Arduino platform. At the entrance to the office, employee applies a card to the
reader, data is being saved into the database, and employee receives message by slack bot with exact time of work day end.
When he leaves work, he will do the same procedure and get worked time. The administrator in theweb panel sees the list
of employees with their statistics.
