import express from 'express';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import APIError from '../helpers/APIError';
import PermChecker from '../helpers/permissonChecker';
import barAdmCtrl from '../controllers/barAdmin.controller';
import config from '../../config/config';

const rolesLevel = {
    superadmin: 1,
    owner: 2,
    administrator: 3,
    customer: 4
}

const router = express.Router(); // eslint-disable-line new-cap

/** Load bar when API with barId route parameter is hit */
router.param('adminId', barAdmCtrl.loadAdmin);

router.route('/')
/** GET /api/bars/:barId/admins - Get bar admins */
    .get(barAdmCtrl.getAdmins)

router.route('/:adminId')
/** GET /api/bars/:barId/admins/:adminId - Get bar admin by id */
    .get(barAdmCtrl.getBarAdmin)
/** PUT /api/bars/:barId/admins/:adminId - Edit bar admin */
    .put(barAdmCtrl.editBarAdmin)


router
/** Middleware for checking permissions*/
    .use((req, res, next) => {
        const decoded = jwt.verify(req.headers.token, config.jwtSecret);
        const reqBarId = req.baseUrl.split('/')[3]
        if (rolesLevel[decoded.role] < 3) {
            PermChecker.check(decoded.role, decoded.userId, reqBarId)
                .then(check => {
                    if (check) {
                        next();
                    } else next(new APIError('Permission denied.', 'Wrong token', httpStatus.FORBIDDEN, true));
                })
        } else throw new APIError('Permission denied.', 'Wrong token', httpStatus.FORBIDDEN, true);
    });

router.route('/')
/** POST /api/bars/:barId/admins - Add new bar admin */
    .post(barAdmCtrl.addAdminToBar)

router.route('/:adminId')
    /** DELETE /api/bars/:barId/admins/:adminId - Remove bar admin */
    .delete(barAdmCtrl.removeAdminFromBar)

export default router;
