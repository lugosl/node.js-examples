import express from 'express';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import validate from 'express-validation';
import APIError from '../helpers/APIError';
import userCtrl from '../controllers/user.controller'
import paramValidation from '../../config/param-validation';
import config from '../../config/config';

const router = express.Router();

/** Load user when API with userId route parameter is hit */
router.param('userId', userCtrl.load);

router.route('/')
/** POST /api/users - Create new user */
    .post(validate(paramValidation.updateUser), userCtrl.create)

router
/** Middleware for checking permissions */
    .use((req, res, next) => {
        const decoded = jwt.verify(req.headers.token, config.jwtSecret);
        if (decoded.role === 'superadmin' || req.url.replace('/', '') == decoded.userId) {
            next();
        } else throw new APIError('Permission denied.', 'Wrong token', httpStatus.FORBIDDEN, true);
    });

router.route('/:userId')
/** GET /api/users/:userId - Get user data by user id*/
    .get(userCtrl.get);

router.route('/:userId')
/** PUT /api/users/:userId - Update user by id */
    .put(validate(paramValidation.updateUser), userCtrl.update)
    /** DELETE /api/users/:userId - Delete user by id */
    .delete(userCtrl.remove);

router
/** Middleware for checking if logged in */
    .use((req, res, next) => {
        const decoded = jwt.verify(req.headers.token, config.jwtSecret);
        if (decoded.role === 'superadmin') {
            next();
        } else throw new APIError('Permission denied.', 'Wrong token', httpStatus.FORBIDDEN, true);
    });

router.route('/:userId/role')
/** POST /api/users/:userId/role - Set user role */
    .post(userCtrl.setRole)

router.route('/')
/** GET /api/users - Get user list */
    .get(userCtrl.list)

router.route('/roles')
/** GET /api/users/roles - Get all user roles */
    .get(userCtrl.getRoles)

export default router;
