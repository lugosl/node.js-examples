import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import authCtrl from '../controllers/auth.controller'

const router = express.Router();

// mount auth routes at /auth
router.route('/login')
/** POST /api/users/auth/login - authentication */
    .post(validate(paramValidation.login), authCtrl.login)

export default router;