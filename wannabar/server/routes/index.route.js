import express from 'express';
import authRoutes from './auth.route';
import userRoutes from './user.route';
import barRoutes from './bar.route';

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
    res.send('OK')
);

// mount auth routes at /auth
router.use('/auth', authRoutes);

// mount user routes at /users
router.use('/users', userRoutes);

// mount user routes at /bars
router.use('/bars', barRoutes);

/* TODO My account (my bars, etc)*/

export default router;
