import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import Filter from '../models/filter.model'

let filterCtrl = {}

filterCtrl.create = function (req, res, next) {
    const {name, type, length} = req.body;
    if (!name||!type||!length) throw new APIError('Bad request!', 'ParamsError', httpStatus.BAD_REQUEST)
    Filter
        .addFilter(name, type, length)
        .then(filters => res.json(filters))
        .catch(e => next(e));
}

filterCtrl.list = function (req, res, next) {
    Filter
        .filterList()
        .then(filters => {
            res.json(filters)
        })
        .catch(e => next(e));
}

filterCtrl.update = function (req, res, next) {
    let filter = {
        name: req.body.name,
        type: req.body.type,
        newName: req.body.newName ? req.body.newName : req.body.name,
        length: req.body.length
    }

    Filter
        .updateFilter(filter)
        .then(filters => res.json(filters))
        .catch(e => next(e));
}

filterCtrl.remove = function (req, res, next) {
    const {name} = req.body;
    if (!name) throw new APIError('Bad request!', 'ParamsError', httpStatus.BAD_REQUEST)
    Filter
        .removeFilter(name)
        .then(filters => res.json(filters))
        .catch(e => next(e));
}

export default filterCtrl