import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/config';
import User from '../models/user.model'

function login(req, res, next) {
    User.getUserByLogin(req.body.login)
        .then((user) => {
            if (passwordHash.verify(req.body.password, user.password)) {
                const token = jwt.sign({
                    login: user.login,
                    userId: user.id,
                    role: user.role
                }, config.jwtSecret, { expiresIn: '10h' });
                return res.json({
                    token,
                    login: user.login,
                });
            }
            throw new APIError('Authentication error. Wrong password', undefined, httpStatus.UNAUTHORIZED, true);
        })
        .catch(e => next(e));
}

export default {login}
