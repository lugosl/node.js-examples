import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import APIError from '../helpers/APIError';
import Bar from '../models/bar.model'
import BarAdmin from '../models/barAdmin.model'
import User from '../models/user.model'
import geo from '../helpers/geocoding'
import config from '../../config/config';


let barCtrl = {}

barCtrl.load = function (req, res, next, id) {
    const request = req;
    Bar.get(id)
        .then(bar => {
            request.bar = bar;
            return next();
        })
}

/**
 * Get barCtrl
 * @returns {Bar}
 */
barCtrl.get = function (req, res) {
    return res.json(req.bar);
}

/**
 * Create new bar
 * @returns {Bar}
 */
barCtrl.create = function (req, res, next) {
    const decoded = jwt.verify(req.headers.token, config.jwtSecret);
    const bar = new Bar({
        name: req.body.name,
        description: req.body.description,
        photo: req.body.photo,
        lat: req.body.lat,
        lng: req.body.lng,
        status: 'not approved',
        owner: req.body.owner ? req.body.owner : decoded.userId,
    });

    geo.reverseGeocode(bar.bar.lat, bar.bar.lng, (err,data) => {
        data.results.map((el) => {
            if (el.types.indexOf('locality') > -1){
                bar.bar.city = el.address_components[0].long_name
                bar.save()
                    .then(saved => {
                        bar.bar.id = saved.insertId
                        res.json(bar.bar);
                    })
                    .catch(e => next(e));
            }
        })
    })
}

/**
 * Remove bar
 * @returns {Bar}
 */
barCtrl.remove = function (req, res, next) {
    const bar = req.bar;
    if (!bar) throw new APIError('Bar doesn\'t exists!', 'ExistError', httpStatus.NOT_FOUND)
    Bar
        .remove(bar.id)
        .then(deletedBar => res.json(bar))
        .catch(e => next(e));
}

/**
 * Update user
 * @returns {Bar}
 */
barCtrl.update = function (req, res, next) {
    const bar = req.bar;
    if (!bar) throw new APIError('Bar doesn\'t exists!', 'ExistError', httpStatus.NOT_FOUND)
    delete req.body.status
    delete req.body.owner
    Bar
        .update(bar.id, req.body)
        .then(updatedBar => res.json(updatedBar))
        .catch(e => next(e));
}

/**
 * Get barCtrl
 * @returns {Bar}
 */
barCtrl.searchList = function (req, res, next) {
    // const {lat = null, lng = null, distance = 5 } = req.query;
    Bar.list(req.query)
        .then(bars => {
            res.json(bars)
        })
        .catch(e => next(e));
}

/**
 * Get barCtrl
 * @returns {Bar}
 */
barCtrl.nearestBar = function (req, res, next) {
    const {lat, lng} = req.query;
    Bar.nearestBar(lat, lng)
        .then(bar => {
            res.json(bar)
        })
        .catch(e => next(e));
}

barCtrl.dutyToday = function (req, res, next) {
    BarAdmin.adminsList(req.bar.id, true)
        .then(bars => {
            res.json(bars)
        })
        .catch(e => next(e));
}

/**
 * Update user role and bar status
 * @returns {Bar}
 */
barCtrl.approve = function (req, res, next) {
    const bar = req.bar;
    if (!bar) throw new APIError('Bar doesn\'t exists!', 'ExistError', httpStatus.NOT_FOUND)
    User.setUserRole(bar.owner, 'owner')
        .catch(e => next(e));
    Bar
        .update(bar.id, {status: 'approved'})
        .then(updatedBar => res.json(updatedBar))
        .catch(e => next(e));
}

barCtrl.adminBarList = function (req, res, next) {
    const {status = 'all'} = req.query;
    Bar.superadminBarList({status})
        .then(bars => {
            res.json(bars)
        })
        .catch(e => next(e));
}


export default barCtrl
