import Promise from 'bluebird';
import Bar from '../models/bar.model'
import BarAdmin from '../models/barAdmin.model'


class PermChecker {
    static check(role, userId, barId) {
        return new Promise(function(resolve, reject) {
            switch (role){
                case 'superadmin':
                    resolve(true)
                case 'owner':
                    return Bar.get(barId).then(bar => {
                        if (bar.owner == userId){
                            resolve(true)
                        } else {
                            resolve(false)
                        }
                    })
                        .catch(e => false)
                case 'administrator':
                    return BarAdmin.adminsList(barId).then(admins =>{
                        admins.map(admin => {
                            if (admin.id == userId){
                                resolve(true)
                            }
                        })
                        resolve(false)
                    })
                        .catch(e => resolve(false))
                default:
                    resolve(false)
            }
        })
    }
}

export default PermChecker