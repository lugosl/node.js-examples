import Promise from 'bluebird';
import db from '../../dbconnection'
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import User from './user.model'

const tableName = 'barAdmins'

class BarAdmin {
    constructor(barAdmin){
        this.barAdmin = barAdmin
    }

    static get(id, barId){
        return new Promise(function(resolve, reject) {
            return db.query('SELECT * FROM '+tableName+' WHERE user_id = ? AND bar_id = ?', [id, barId], function (error, results) {
                if (error) reject(error);
                if (!results[0]) reject(new APIError('No such bar administrator exists!', 'ExistError', httpStatus.NOT_FOUND, true))
                else{
                    resolve(results[0])
                }
            } )
        })
            .then(admin => {
                return User.get(admin.user_id)
                    .then(user => {
                        user.workdays = admin.workdays
                        return user
                })
            })
    }

    static addBarAdmin(userId, barId){
        let sql = "INSERT INTO "+tableName+
            " (user_id, bar_id)" +
            " VALUES ?"
        let values = [[...Object.values({userId,barId})]]
        return new Promise(function(resolve, reject) {
            return db.query(sql, [values], function (error, results) {
                if (error) reject(new APIError(error.sqlMessage, "Duplicate exists field", httpStatus.BAD_REQUEST));
                resolve(results)
            } )
        })
    }

    static adminsList(barId, duty = false){
        return new Promise(function(resolve, reject) {
            return db.query('SELECT * FROM '+tableName+' WHERE bar_id = ?', [barId], function (error, results) {
                if (error) reject(new APIError(error.sqlMessage, "Can't select admins list", httpStatus.BAD_REQUEST));
                resolve(results)
            } )
        })
            .then(admins => {
                if (duty) {
                    admins = admins.filter(admin => {
                        const workdays = admin.workdays
                        const today = new Date()
                        if (workdays && workdays.indexOf(today.getDay()) > -1){
                            return true
                        }
                        return false
                    })
                }
                let usersGetPromises =  admins.map(admin => {
                    return User.get(admin.user_id)
                        .then(user => {
                            user.workdays = admin.workdays
                            return user
                        })
                })
                return Promise.all(usersGetPromises)
            })
    }

    static removeBarAdmin(userId, barId){
        return new Promise(function(resolve, reject) {
            return db.query('DELETE FROM '+tableName+' WHERE user_id = ? AND bar_id = ?', [userId, barId], function (error, results) {
                if (error) reject(error);
                resolve(results)
            } )
        })
            .then(result => {
                return this.adminsList(barId)
            })
    }

    static updateBarAdmin(id, barId, obj){
        let fieldsToUpdate = ''
        Object.keys(obj).map(function(key, index) {
            let coma = (Object.keys(obj).length === index + 1)? '' : ','
            fieldsToUpdate += key +' = \''+ obj[key] +'\'' + coma
        });
        return new Promise(function(resolve, reject) {
            return db.query('UPDATE '+tableName+' SET '+fieldsToUpdate+' WHERE user_id = ? AND bar_id = ?', [id, barId], (error, results, fields) => {
                if (error) {
                    switch (error.code){
                        case 'ER_PARSE_ERROR':
                            reject(new APIError('API recieved wrong fields!', "Wrong property", httpStatus.BAD_REQUEST, true))
                            break
                        case 'ER_DUP_ENTRY':
                            reject(new APIError(error.sqlMessage, "Duplicate exists field", httpStatus.BAD_REQUEST, true));
                            break
                        default:
                            reject(error);
                    }
                }
                resolve(results)
            })
        }).then(results => {
            return this.get(id, barId)
        })
    }


    save(){
        let bar = this.bar
        let fieldsToSave = ''
        Object.keys(bar).map(function(key, index) {
            let coma = (Object.keys(bar).length === index + 1)? '' : ','
            fieldsToSave += key + coma
        });
        let sql = "INSERT INTO "+tableName+
            " ("+fieldsToSave+")" +
            " VALUES ?"
        let values = [[...Object.values(bar)]]
        return new Promise(function(resolve, reject) {
            return db.query(sql, [values], function (error, results) {
                if (error) reject(new APIError(error.sqlMessage, "Duplicate exists field", httpStatus.BAD_REQUEST));
                resolve(results)
            } )
        })
    }
}

export default BarAdmin