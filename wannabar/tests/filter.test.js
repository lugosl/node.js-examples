import chai, { expect } from 'chai'
import chaiHttp from 'chai-http'
import server from '../index';
let should = chai.should();

chai.use(chaiHttp);

let supertoken = '';

describe('## Filters APIs', () => {

    describe('/GET /api/health-check.', () => {
        it('it should GET status OK(200)', (done) => {
            chai.request(server)
                .get('/api/health-check')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    let loginData ={
        login: 'superadmin',
        password: '1111'
    }

    describe('# POST /api/auth/login', () => {
        it('should login user and return supertoken', (done) => {
            chai.request(server)
                .post(`/api/auth/login`)
                .send(loginData)
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token')
                    res.body.login.should.eql('superadmin')
                    supertoken = res.body.token
                    done();
                })
                .catch(done);
        });
    });

    describe('# GET /api/bars/filters', () => {
        it('should return list of bars possible filters', (done) => {
            chai.request(server)
                .get(`/api/bars/filters`)
                .set('token', supertoken)
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.be.a('object');
                    res.body[0].should.have.property('position')
                    res.body[0].should.have.property('name')
                    res.body[0].should.have.property('type')
                    done();
                })
                .catch(done);
        });
    });

    describe('# POST /api/bars/filters', () => {
        it('should create new bar filter', (done) => {
            chai.request(server)
                .post(`/api/bars/filters`)
                .set('token', supertoken)
                .send({
                    name: "autotest",
                    type: "INT",
                    length: 11
                })
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[res.body.length - 1].should.be.a('object');
                    res.body[res.body.length - 1].should.have.property('position').eq(res.body.length + 1)
                    res.body[res.body.length - 1].should.have.property('name').eq('autotest')
                    res.body[res.body.length - 1].should.have.property('type').eq('int(11)')
                    done();
                })
                .catch(done);
        });
    });

    describe('# PUT /api/bars/filters', () => {
        it('should update bar filter by filter params', (done) => {
            chai.request(server)
                .put(`/api/bars/filters`)
                .set('token', supertoken)
                .send({
                    name: "autotest",
                    newName: "testauto",
                    type: "INT",
                    length: 11
                })
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[res.body.length - 1].should.be.a('object');
                    res.body[res.body.length - 1].should.have.property('position').eq(res.body.length + 1)
                    res.body[res.body.length - 1].should.have.property('name').eq('testauto')
                    res.body[res.body.length - 1].should.have.property('type').eq('int(11)')
                    done();
                })
                .catch(done);
        });
    });

    describe('# DELETE /api/bars/filters', () => {
        it('should remove bar filter by filter name', (done) => {
            chai.request(server)
                .delete(`/api/bars/filters`)
                .set('token', supertoken)
                .send({
                    name: "testauto"
                })
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[res.body.length - 1].should.be.a('object');
                    res.body[res.body.length - 1].should.have.property('position')
                    res.body[res.body.length - 1].should.have.property('name').not.eq('testauto')
                    res.body[res.body.length - 1].should.have.property('type').not.eq('int(11)')
                    done();
                })
                .catch(done);
        });
    });
})