import chai, { expect } from 'chai'
import chaiHttp from 'chai-http'
import server from '../index';
let should = chai.should();

chai.use(chaiHttp);

/* token for auth. Expiration time - 1000 days. No TOKEN */
let supertoken = '';

describe('## SuperAdmin APIs', () => {

    describe('/GET /api/health-check.', () => {
        it('it should GET status OK(200)', (done) => {
            chai.request(server)
                .get('/api/health-check')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    let loginData ={
        login: 'superadmin',
        password: '1111'
    }

    describe('# POST /api/auth/login', () => {
        it('should login user and return supertoken', (done) => {
            chai.request(server)
                .post(`/api/auth/login`)
                .send(loginData)
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token')
                    res.body.login.should.eql('superadmin')
                    supertoken = res.body.token
                    done();
                })
                .catch(done);
        });
    });

    describe('# GET /api/users/:userId', () => {

        it('should report error with message - No such user exists, when user does not exists', (done) => {
            chai.request(server)
                .get('/api/users/0')
                .set('token', supertoken)
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.name.should.eql('ExistError')
                    res.body.message.should.eql('No such user exists!')
                    done();
                })
                .catch(done);
        });
    });

    describe('# PUT /api/users/:userId', () => {
        it('should return User exists error', (done) => {
            chai.request(server)
                .put(`/api/users/0`)
                .set('token', supertoken)
                .send({
                    email: 'autotest@testmail.com',
                    login: 'autotest'
                })
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.name.should.eql('ExistError')
                    res.body.message.should.eql("No such user exists!")
                    done();
                })
                .catch(done);
        });
    });

    describe('# GET /api/users/', () => {
        it('should get all users', (done) => {
            chai.request(server)
                .get('/api/users')
                .set('token', supertoken)
                .then((res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.be.a('object');
                    res.body[0].should.have.property('name')
                    res.body[0].should.have.property('surname')
                    res.body[0].should.have.property('email')
                    res.body[0].should.have.property('mobileNumber')
                    res.body[0].should.have.property('login')
                    res.body[0].should.have.property('role')
                    done();
                })
                .catch(done);
        });

        it('should get all users by role', (done) => {
            chai.request(server)
                .get('/api/users')
                .set('token', supertoken)
                .query({ role: 'customer' })
                .then((res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.be.a('object');
                    res.body[0].should.have.property('name')
                    res.body[0].should.have.property('surname')
                    res.body[0].should.have.property('email')
                    res.body[0].should.have.property('mobileNumber')
                    res.body[0].should.have.property('login')
                    res.body[0].should.have.property('role').eql(4)
                    done();
                })
                .catch(done);
        });
    });

    describe('# GET /api/bars/', () => {
        it('should get all bars', (done) => {
            chai.request(server)
                .get('/api/bars')
                .set('token', supertoken)
                .then((res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.be.a('object');
                    res.body[0].should.have.property('name')
                    res.body[0].should.have.property('description')
                    res.body[0].should.have.property('photo')
                    res.body[0].should.have.property('lat')
                    res.body[0].should.have.property('lng')
                    res.body[0].should.have.property('owner')
                    done();
                })
                .catch(done);
        });

        it('should get all bars by status', (done) => {
            chai.request(server)
                .get('/api/bars')
                .set('token', supertoken)
                .query({ status: 'approved' })
                .then((res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.be.a('object');
                    res.body[0].should.have.property('name')
                    res.body[0].should.have.property('description')
                    res.body[0].should.have.property('photo')
                    res.body[0].should.have.property('lat')
                    res.body[0].should.have.property('lng')
                    res.body[0].should.have.property('owner')
                    done();
                })
                .catch(done);
        });
    });

    describe('# POST /api/users/6/role', () => {
        it('should change user role', (done) => {
            chai.request(server)
                .post(`/api/users/6/role`)
                .set('token', supertoken)
                .send({role: 'customer'})
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name')
                    res.body.should.have.property('surname')
                    res.body.should.have.property('email')
                    res.body.should.have.property('mobileNumber')
                    res.body.should.have.property('login')
                    res.body.should.have.property('role').eql(4)
                    done();
                })
                .catch(done);
        });
    });

})