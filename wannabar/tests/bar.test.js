import chai, { expect } from 'chai'
import chaiHttp from 'chai-http'
import server from '../index';
let should = chai.should();

chai.use(chaiHttp);

/* token for auth. No TOKEN */
let supertoken = '';
let token = '';

describe('## Bar APIs', () => {

    describe('/GET /api/health-check.', () => {
        it('it should GET status OK(200)', (done) => {
            chai.request(server)
                .get('/api/health-check')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    describe('# POST /api/auth/login', () => {
        it('getting supertoken for next tests', (done) => {
            chai.request(server)
                .post(`/api/auth/login`)
                .send({
                    login: 'superadmin',
                    password: '1111'
                })
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token')
                    res.body.login.should.eql('superadmin')
                    supertoken = res.body.token
                    done();
                })
                .catch(done);
        });

        it('getting user token for next tests', (done) => {
            chai.request(server)
                .post(`/api/auth/login`)
                .send({
                    login: 'customer2',
                    password: '1111'
                })
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token')
                    res.body.login.should.eql('customer2')
                    token = res.body.token
                    done();
                })
                .catch(done);
        });
    });

    let bar = {
        'name': 'First Bar',
        'description': 'test description',
        'city': 'Kharkiv',
        'photo': 'autotest.png',
        'lat': 49.981044,
        'lng': 36.261606,
    };

    describe('# POST /api/bars', () => {
        it('should try to create new bar and return error Duplicate exists field', (done) => {
            chai.request(server)
                .post('/api/bars')
                .set('token', token)
                .send(bar)
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.name.should.eql('Duplicate exists field')
                    done();
                })
                .catch(done);
        });

        it('should create new bar and return it', (done) => {
            bar.name = "Test Bar"
            chai.request(server)
                .post('/api/bars')
                .set('token', token)
                .send(bar)
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id')
                    res.body.should.have.property('name').eq(bar.name)
                    res.body.should.have.property('description').eq(bar.description)
                    res.body.should.have.property('city').eq(bar.city)
                    res.body.should.have.property('lat').eq(bar.lat)
                    res.body.should.have.property('lng').eq(bar.lng)
                    res.body.should.have.property('status').eq('not approved')
                    res.body.should.have.property('photo').eq(bar.photo)
                    bar.id = res.body.id
                    done();
                })
                .catch(done);
        });

        it('should approve bar by superadmin', (done) => {
            chai.request(server)
                .post(`/api/bars/${bar.id}/approve`)
                .set('token', supertoken)
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id')
                    res.body.should.have.property('name').eq(bar.name)
                    res.body.should.have.property('description').eq(bar.description)
                    res.body.should.have.property('city').eq(bar.city)
                    res.body.should.have.property('lat').eq(bar.lat)
                    res.body.should.have.property('lng').eq(bar.lng)
                    res.body.should.have.property('status').eq('approved')
                    res.body.should.have.property('photo').eq(bar.photo)
                    done();
                })
                .catch(done);
        });
    });

    describe('# POST /api/auth/login owner token', () => {
        it('getting owner token for tests', (done) => {
            chai.request(server)
                .post(`/api/auth/login`)
                .send({
                    login: 'customer2',
                    password: '1111'
                })
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token')
                    res.body.login.should.eql('customer2')
                    token = res.body.token
                    done();
                })
                .catch(done);
        });
    });

    describe('# POST /api/bars/:barId', () => {
        it('edit bar data', (done) => {
            chai.request(server)
                .put(`/api/bars/${bar.id}`)
                .set('token', token)
                .send({
                    name: 'First Bar ;)',
                    description: 'test description1',
                })
                .then((res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id')
                    res.body.should.have.property('name').eq('First Bar ;)')
                    res.body.should.have.property('description').eq("test description1")
                    res.body.should.have.property('city').eq(bar.city)
                    res.body.should.have.property('lat').eq(bar.lat)
                    res.body.should.have.property('lng').eq(bar.lng)
                    res.body.should.have.property('status').eq('approved')
                    res.body.should.have.property('photo').eq(bar.photo)
                    bar.name = 'First Bar ;)'
                    bar.description = "test description1"
                    done();
                })
                .catch(done);
        });
    });

    let user = {
        name: 'autotest',
        surname: 'autotest',
        mobileNumber: '0979797977',
        email: "autotestAdm@example.com",
        login: "autotestAdm",
        password: '1111'
    }

    describe('# POST /api/bars/:barId/admins', () => {
        it('should add admin to bar', (done) => {
            chai.request(server)
                .post(`/api/bars/${bar.id}/admins`)
                .set('token', token)
                .send(user)
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.be.a('object');
                    res.body[0].should.have.property('id');
                    res.body[0].should.have.property('name').eql(user.name);
                    res.body[0].should.have.property('surname').eql(user.surname);
                    res.body[0].should.have.property('email').eql(user.email);
                    res.body[0].should.have.property('mobileNumber').eql(user.mobileNumber);
                    res.body[0].should.have.property('login').eql(user.login);
                    res.body[0].should.have.property('role').eql(3)
                    user.id = res.body[0].id
                    done();
                })
                .catch(done);
        });
    });

    describe('# GET /api/bars/:barId/admins', () => {
        it('should return array of bar admins', (done) => {
            chai.request(server)
                .get(`/api/bars/${bar.id}/admins`)
                .set('token', token)
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.be.a('object');
                    res.body[0].should.have.property('id');
                    res.body[0].should.have.property('name').eql(user.name);
                    res.body[0].should.have.property('surname').eql(user.surname);
                    res.body[0].should.have.property('email').eql(user.email);
                    res.body[0].should.have.property('mobileNumber').eql(user.mobileNumber);
                    res.body[0].should.have.property('login').eql(user.login);
                    res.body[0].should.have.property('role').eql(3)
                    user.id = res.body[0].id
                    done();
                })
                .catch(done);
        });
    });

    describe('# GET /api/bars/:barId/admins/:adminId', () => {
        it('should return object of bar admin', (done) => {
            chai.request(server)
                .get(`/api/bars/${bar.id}/admins/${user.id}`)
                .set('token', token)
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id');
                    res.body.should.have.property('name').eql(user.name);
                    res.body.should.have.property('surname').eql(user.surname);
                    res.body.should.have.property('email').eql(user.email);
                    res.body.should.have.property('mobileNumber').eql(user.mobileNumber);
                    res.body.should.have.property('login').eql(user.login);
                    res.body.should.have.property('role').eql(3)
                    done();
                })
                .catch(done);
        });
    });

    describe('# PUT /api/bars/:barId/admins/:adminId', () => {
        it('should update bar admin workdays', (done) => {
            chai.request(server)
                .put(`/api/bars/${bar.id}/admins/${user.id}`)
                .set('token', token)
                .send({workdays: "1,2,4,5,7"})
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id');
                    res.body.should.have.property('name').eql(user.name);
                    res.body.should.have.property('surname').eql(user.surname);
                    res.body.should.have.property('email').eql(user.email);
                    res.body.should.have.property('mobileNumber').eql(user.mobileNumber);
                    res.body.should.have.property('login').eql(user.login);
                    res.body.should.have.property('role').eql(3)
                    res.body.should.have.property('workdays').eql("1,2,4,5,7")
                    done();
                })
                .catch(done);
        });
    });

    describe('# DELETE /api/bars/:barId/admins/:adminId', () => {
        it('should delete bar admin', (done) => {
            chai.request(server)
                .delete(`/api/bars/${bar.id}/admins/${user.id}`)
                .set('token', token)
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                })
                .catch(done);
        });
    });

    describe('# DELETE /api/bars/:barId', () => {
        it('should delete bar', (done) => {
            chai.request(server)
                .delete(`/api/bars/${bar.id}`)
                .set('token', token)
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id')
                    res.body.should.have.property('name').eq(bar.name)
                    res.body.should.have.property('description').eq(bar.description)
                    res.body.should.have.property('city').eq(bar.city)
                    res.body.should.have.property('lat').eq(bar.lat)
                    res.body.should.have.property('lng').eq(bar.lng)
                    res.body.should.have.property('photo').eq(bar.photo)
                    done();
                })
                .catch(done);
        });

        it('should return Permission error', (done) => {
            chai.request(server)
                .delete(`/api/bars/1`)
                .set('token', token)
                .then((err, res) => {
                    if(!res) res = err
                    res.should.have.status(403);
                    res.body.should.be.a('object');
                    res.body.name.should.eql('Wrong token')
                    res.body.message.should.eql("Permission denied.")
                    done();
                })
                .catch(done);
        });
    });

    describe('# DELETE /api/users/', () => {
        it('should delete test user', (done) => {
            chai.request(server)
                .delete(`/api/users/${user.id}`)
                .set('token', supertoken)
                .then((res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('name').eql(user.name);
                    res.body.should.have.property('surname').eql(user.surname);
                    res.body.should.have.property('mobileNumber').eql(user.mobileNumber);
                    res.body.should.have.property('email').eql(user.email);
                    res.body.should.have.property('login').eql(user.login);
                    res.body.should.have.property('id').eql(user.id);
                    done();
                })
                .catch(done);
        });
    });

});
