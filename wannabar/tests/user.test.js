import chai, { expect } from 'chai'
import chaiHttp from 'chai-http'
import server from '../index';
let should = chai.should();

chai.use(chaiHttp);

/* token for auth. Expiration time - 1000 days. No TOKEN */
let token = '';

describe('## User APIs', () => {

    describe('/GET /api/health-check.', () => {
        it('it should GET status OK(200)', (done) => {
            chai.request(server)
                .get('/api/health-check')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    let user = {
        'name': 'autotest',
        'surname': 'testauto',
        'mobileNumber': '1234567890',
        'email': 'autotest@testmail.com',
        'login': 'autotest',
        'password': '1111'
    };

  describe('# POST /api/users', () => {
      it('should return Duplicate exists field error', (done) => {
          user.login = 'customer'
          chai.request(server)
              .post('/api/users')
              .set('token', token)
              .send(user)
              .then((err, res) => {
                  if(!res) res = err
                  res.should.have.status(400);
                  res.body.should.be.a('object');
                  res.body.name.should.eql('Duplicate exists field')
                  done();
              })
              .catch(done);
      });

      it('should create a new user', (done) => {
          user.login = 'autotest'
          chai.request(server)
            .post('/api/users')
            .set('token', token)
            .send(user)
            .then((err, res) => {
                if(!res) res = err
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('savedUser')
                res.body.should.have.property('token')
                res.body.savedUser.should.have.property('name').eql(user.name);
                res.body.savedUser.should.have.property('surname').eql(user.surname);
                res.body.savedUser.should.have.property('mobileNumber').eql(user.mobileNumber);
                res.body.savedUser.should.have.property('email').eql(user.email);
                res.body.savedUser.should.have.property('login').eql(user.login);
                res.body.savedUser.should.have.property('id');
                res.body.savedUser.should.have.property('role');
                user = res.body.savedUser;
                token = res.body.token
                done();
            })
            .catch(done);
    });
  });

  describe('# GET /api/users/:userId', () => {
    it('should get user details', (done) => {
        chai.request(server)
            .get(`/api/users/${user.id}`)
            .set('token', token)
            .then((err, res) => {
                if(!res) res = err
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('name').eql(user.name);
                res.body.should.have.property('surname').eql(user.surname);
                res.body.should.have.property('mobileNumber').eql(user.mobileNumber);
                res.body.should.have.property('email').eql(user.email);
                res.body.should.have.property('login').eql(user.login);
                res.body.should.have.property('id').eql(user.id);
                res.body.should.have.property('role').eql(user.role);
                done();
            })
            .catch(done);
    });

      it('should return Permission error', (done) => {
          chai.request(server)
              .get(`/api/users/1`)
              .set('token', token)
              .send(user)
              .then((err, res) => {
                  if(!res) res = err
                  res.should.have.status(403);
                  res.body.should.be.a('object');
                  res.body.name.should.eql('Wrong token')
                  res.body.message.should.eql("Permission denied.")
                  done();
              })
              .catch(done);
      });
  });

  describe('# PUT /api/users/:userId', () => {
    it('should update user details', (done) => {
        user.name = 'nametest'
        user.surname = 'surnametest'
        user.password = '1234'
        chai.request(server)
            .put(`/api/users/${user.id}`)
            .set('token', token)
            .send(user)
            .then((err, res) => {
                if(!res) res = err
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('name').eql(user.name);
                res.body.should.have.property('surname').eql(user.surname);
                done();
            })
            .catch(done);
    });

      it('should return Duplicate exists field error', (done) => {
          user.login = 'customer'
          user.email = 'customer@example.com'
          chai.request(server)
              .put(`/api/users/${user.id}`)
              .set('token', token)
              .send(user)
              .then((err, res) => {
                  if(!res) res = err
                  res.should.have.status(400);
                  res.body.should.be.a('object');
                  res.body.name.should.eql('Duplicate exists field')
                  done();
              })
              .catch(done);
      });

      it('should return email format error', (done) => {
          user.email = 'testuser mailforspam.com'
          chai.request(server)
              .put(`/api/users/${user.id}`)
              .set('token', token)
              .send(user)
              .then((err, res) => {
                  if(!res) res = err
                  res.should.have.status(400);
                  res.body.should.be.a('object');
                  res.body.message.should.eql("\"email\" must be a valid email")
                  done();
              })
              .catch(done);
      });

      it('should return Permission error', (done) => {
          chai.request(server)
              .put(`/api/users/1`)
              .set('token', token)
              .send(user)
              .then((err, res) => {
                  if(!res) res = err
                  res.should.have.status(403);
                  res.body.should.be.a('object');
                  res.body.name.should.eql('Wrong token')
                  res.body.message.should.eql("Permission denied.")
                  done();
              })
              .catch(done);
      });
  });

    describe('# POST /api/auth/login', () => {
      it('should login user and return token', (done) => {
          user.login = 'autotest'
          user.email ='autotest@testmail.com'
          chai.request(server)
              .post(`/api/auth/login`)
              .set('token', token)
              .send({
                  "login": user.login,
                  "password": user.password
              })
              .then((res) => {
                  if(!res) res = err
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  res.body.should.have.property('token')
                  res.body.login.should.eql('autotest')
                  done();
              })
              .catch(done);
      });
    });

  describe('# GET /api/users/', () => {
      it('should return Permission error', (done) => {
          chai.request(server)
              .get('/api/users')
              .set('token', token)
              .send(user)
              .then((err, res) => {
                  if(!res) res = err
                  res.should.have.status(403);
                  res.body.should.be.a('object');
                  res.body.name.should.eql('Wrong token')
                  res.body.message.should.eql("Permission denied.")
                  done();
              })
              .catch(done);
      });
  });

  describe('# DELETE /api/users/', () => {
    it('should delete user', (done) => {
        chai.request(server)
            .delete(`/api/users/${user.id}`)
            .set('token', token)
            .then((res) => {
                res.should.have.status(200);
                res.body.should.have.property('name').eql(user.name);
                res.body.should.have.property('surname').eql(user.surname);
                res.body.should.have.property('mobileNumber').eql(user.mobileNumber);
                res.body.should.have.property('email').eql(user.email);
                res.body.should.have.property('login').eql(user.login);
                res.body.should.have.property('id').eql(user.id);
                res.body.should.have.property('role').eql(user.role);
                done();
            })
            .catch(done);
    });
  });
});
